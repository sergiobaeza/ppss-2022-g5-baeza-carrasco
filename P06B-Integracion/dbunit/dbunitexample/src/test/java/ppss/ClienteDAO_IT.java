package ppss;

import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;

import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.junit.jupiter.api.*;

/* IMPORTANTE:
    Dado que prácticamente todos los métodos de dBUnit lanzan una excepción,
    vamos a usar "throws Esception" en los métodos, para que el código quede más
    legible sin necesidad de usar un try..catch o envolver cada sentencia dbUnit
    con un assertDoesNotThrow()
*/
public class ClienteDAO_IT {

    private ClienteDAO clienteDAO; //SUT
    private IDatabaseTester databaseTester;
    private IDatabaseConnection connection;

    @BeforeEach
    public void setUp() throws Exception {

        String cadena_conexionDB = "jdbc:mysql://localhost:3306/?useSSL=false";
        databaseTester = new MiJdbcDatabaseTester("com.mysql.cj.jdbc.Driver",
                "jdbc:mysql://localhost:3306/DBUNIT?useSSL=false", "root", "dss");
        //obtenemos la conexión con la BD
        connection = databaseTester.getConnection();

        clienteDAO = new ClienteDAO();
    }

    @Test
    public void testInsert() throws Exception {
        Cliente cliente = new Cliente(1,"John", "Smith");
        cliente.setDireccion("1 Main Street");
        cliente.setCiudad("Anycity");

        //Inicializamos el dataSet con los datos iniciales de la tabla cliente
        IDataSet dataSet = new FlatXmlDataFileLoader().load("/cliente-init.xml");
        //Inyectamos el dataset en el objeto databaseTester
        databaseTester.setDataSet(dataSet);
        //inicializamos la base de datos con los contenidos del dataset
        databaseTester.onSetup();
        //invocamos a nuestro SUT
        Assertions.assertDoesNotThrow(()->clienteDAO.insert(cliente));

        //recuperamos los datos de la BD después de invocar al SUT
        IDataSet databaseDataSet = connection.createDataSet();
        //Recuperamos los datos de la tabla cliente
        ITable actualTable = databaseDataSet.getTable("cliente");

        //creamos el dataset con el resultado esperado
        IDataSet expectedDataSet = new FlatXmlDataFileLoader().load("/cliente-esperado.xml");
        ITable expectedTable = expectedDataSet.getTable("cliente");

        Assertion.assertEquals(expectedTable, actualTable);

    }

    @Test
    public void testDelete() throws Exception {
        Cliente cliente =  new Cliente(1,"John", "Smith");
        cliente.setDireccion("1 Main Street");
        cliente.setCiudad("Anycity");

        //inicializamos la BD
        IDataSet dataSet = new FlatXmlDataFileLoader().load("/cliente-esperado.xml");
        databaseTester.setDataSet(dataSet);
        databaseTester.onSetup();

        //invocamos a nuestro SUT
        Assertions.assertDoesNotThrow(()->clienteDAO.delete(cliente));

        IDataSet databaseDataSet = connection.createDataSet();
        //Recuperamos los datos de la tabla cliente
        ITable actualTable = databaseDataSet.getTable("cliente");

        //creamos el dataset con el resultado esperado
        IDataSet expectedDataSet = new FlatXmlDataFileLoader().load("/cliente-init.xml");
        ITable expectedTable = expectedDataSet.getTable("cliente");

        Assertion.assertEquals(expectedTable, actualTable);
    }


    @Test
    public void testInsert2() throws Exception {
        Cliente cliente = new Cliente(3,"Paco", "Martinez");
        cliente.setDireccion("Doctor Pizarro");
        cliente.setCiudad("Barcelona");

        //Inicializamos el dataSet con los datos iniciales de la tabla cliente
        IDataSet dataSet = new FlatXmlDataFileLoader().load("/cliente-init2.xml");
        //Inyectamos el dataset en el objeto databaseTester
        databaseTester.setDataSet(dataSet);
        //inicializamos la base de datos con los contenidos del dataset
        databaseTester.onSetup();
        //invocamos a nuestro SUT
        Assertions.assertDoesNotThrow(()->clienteDAO.insert(cliente));

        //recuperamos los datos de la BD después de invocar al SUT
        IDataSet databaseDataSet = connection.createDataSet();
        //Recuperamos los datos de la tabla cliente
        ITable actualTable = databaseDataSet.getTable("cliente");

        //creamos el dataset con el resultado esperado
        IDataSet expectedDataSet = new FlatXmlDataFileLoader().load("/cliente-esperado2.xml");
        ITable expectedTable = expectedDataSet.getTable("cliente");

        Assertion.assertEquals(expectedTable, actualTable);

    }


    @Test
    public void testUpdate() throws Exception {
        int clienteId = 1;

        //Inicializamos el dataSet con los datos iniciales de la tabla cliente
        IDataSet dataSet = new FlatXmlDataFileLoader().load("/cliente-init3.xml");
        //Inyectamos el dataset en el objeto databaseTester
        databaseTester.setDataSet(dataSet);
        //inicializamos la base de datos con los contenidos del dataset
        databaseTester.onSetup();
        //invocamos a nuestro SUT


        Cliente c = Assertions.assertDoesNotThrow( () ->  clienteDAO.retrieve(clienteId));

        // Editamos los datos
        c.setCiudad("NewCity");
        c.setDireccion("Other Street");

        Assertions.assertDoesNotThrow( () -> clienteDAO.update(c));

        //recuperamos los datos de la BD después de invocar al SUT
        IDataSet databaseDataSet = connection.createDataSet();
        //Recuperamos los datos de la tabla cliente
        ITable actualTable = databaseDataSet.getTable("cliente");

        //creamos el dataset con el resultado esperado
        IDataSet expectedDataSet = new FlatXmlDataFileLoader().load("/cliente-esperado3.xml");
        ITable expectedTable = expectedDataSet.getTable("cliente");

        Assertion.assertEquals(expectedTable, actualTable);

    }

    @Test
    public void testRetrieve() throws Exception {
        int clienteId = 1;
        Cliente cEsperado = new Cliente(1, "John", "Smith");
        cEsperado.setDireccion("1 Main Street");
        cEsperado.setCiudad("Anycity");

        //Inicializamos el dataSet con los datos iniciales de la tabla cliente
        IDataSet dataSet = new FlatXmlDataFileLoader().load("/cliente-init3.xml");
        //Inyectamos el dataset en el objeto databaseTester
        databaseTester.setDataSet(dataSet);
        //inicializamos la base de datos con los contenidos del dataset
        databaseTester.onSetup();
        //invocamos a nuestro SUT


        Cliente c = Assertions.assertDoesNotThrow( () ->  clienteDAO.retrieve(clienteId));



        Assertions.assertAll(
                () -> Assertions.assertEquals(cEsperado.getId(), c.getId()),
                () -> Assertions.assertEquals(cEsperado.getNombre(), c.getNombre()),
                () -> Assertions.assertEquals(cEsperado.getApellido(), c.getApellido()),
                () -> Assertions.assertEquals(cEsperado.getDireccion(), c.getDireccion()),
                () -> Assertions.assertEquals(cEsperado.getCiudad(), c.getCiudad()));

    }

}
