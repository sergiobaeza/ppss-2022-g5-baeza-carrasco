package ppss.matriculacion.dao;

import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;

import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import ppss.matriculacion.to.AlumnoTO;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;


public class AlumnoDAOIT {
    private IAlumnoDAO alumnoDAO; //SUT
    private AlumnoTO alu;
    private IDatabaseTester databaseTester;
    private IDatabaseConnection connection;

    @BeforeEach
    public void setUp() throws Exception {

        String cadena_conexionDB = "jdbc:mysql://localhost:3306/?useSSL=false";
        databaseTester = new MiJdbcDatabaseTester("com.mysql.cj.jdbc.Driver",
                "jdbc:mysql://localhost:3306/matriculacion?useSSL=false", "root", "dss");
        //obtenemos la conexión con la BD
        connection = databaseTester.getConnection();

        alumnoDAO = new FactoriaDAO().getAlumnoDAO();
        alu = new AlumnoTO();
    }

    @Test
    @Tag("Integracion-fase1")
    public void testA1() throws Exception {
        // Inicializamos la tabla2xml como base de datos
        IDataSet ds = new FlatXmlDataFileLoader().load("/tabla2.xml");
        databaseTester.setDataSet(ds);
        databaseTester.onSetup();

        AlumnoTO alu = new AlumnoTO();
        alu.setNif("33333333C");
        alu.setNombre("Elena Aguirre Juarez");
        alu.setFechaNacimiento(LocalDate.of(1985, 2, 22));

        // Introduzco los datos
        assertDoesNotThrow( () -> alumnoDAO.addAlumno(alu));


        // Obtengo la base de datos actual
        IDataSet databaseDataSet = connection.createDataSet();
        ITable actualTable = databaseDataSet.getTable("alumnos");

        // Obtengo la base de datos del fichero
        IDataSet expectedDataSet = new FlatXmlDataFileLoader().load("/tabla3.xml");
        ITable expectedTable = expectedDataSet.getTable("alumnos");


        // Comprobaciones
        Assertion.assertEquals(expectedTable, actualTable);
    }

    @Test
    @Tag("Integracion-fase1")
    public void testA2() throws Exception{
        // Inicializamos la tabla2xml como base de datos
        IDataSet ds = new FlatXmlDataFileLoader().load("/tabla2.xml");
        databaseTester.setDataSet(ds);
        databaseTester.onSetup();

        AlumnoTO alu = new AlumnoTO();
        alu.setNif("11111111A");
        alu.setNombre("Alfonso Ramirez Ruiz");
        alu.setFechaNacimiento(LocalDate.of(1982, 2, 22));

        // Introduzco los datos
        DAOException e = assertThrows(DAOException.class, () -> alumnoDAO.addAlumno(alu));

        assertEquals("Error al conectar con BD", e.getMessage());

    }

    @Test
    @Tag("Integracion-fase1")
    public void testA3() throws Exception{
        // Inicializamos la tabla2xml como base de datos
        IDataSet ds = new FlatXmlDataFileLoader().load("/tabla2.xml");
        databaseTester.setDataSet(ds);
        databaseTester.onSetup();

        AlumnoTO alu = new AlumnoTO();
        alu.setNif("44444444D");
        alu.setNombre(null);
        alu.setFechaNacimiento(LocalDate.of(1982, 2, 22));

        // Introduzco los datos
        DAOException e = assertThrows(DAOException.class, () -> alumnoDAO.addAlumno(alu));

        assertEquals("Error al conectar con BD", e.getMessage());

    }

    @Test
    @Tag("Integracion-fase1")
    public void testA4() throws Exception{
        // Inicializamos la tabla2xml como base de datos
        IDataSet ds = new FlatXmlDataFileLoader().load("/tabla2.xml");
        databaseTester.setDataSet(ds);
        databaseTester.onSetup();

        AlumnoTO alu = null;

        // Introduzco los datos
        DAOException e = assertThrows(DAOException.class, () -> alumnoDAO.addAlumno(alu));

        assertEquals("Alumno nulo", e.getMessage());

    }

    @Test
    @Tag("Integracion-fase1")
    public void testA5() throws Exception{
        // Inicializamos la tabla2xml como base de datos
        IDataSet ds = new FlatXmlDataFileLoader().load("/tabla2.xml");
        databaseTester.setDataSet(ds);
        databaseTester.onSetup();

        AlumnoTO alu = new AlumnoTO();
        alu.setNif(null);
        alu.setNombre("Pedro Garcia Lopez");
        alu.setFechaNacimiento(LocalDate.of(1982, 2, 22));

        // Introduzco los datos
        DAOException e = assertThrows(DAOException.class, () -> alumnoDAO.addAlumno(alu));

        assertEquals("Error al conectar con BD", e.getMessage());

    }

    @Test
    @Tag("Integracion-fase1")
    public void testB1() throws Exception {
        // Inicializamos la tabla2xml como base de datos
        IDataSet ds = new FlatXmlDataFileLoader().load("/tabla2.xml");
        databaseTester.setDataSet(ds);
        databaseTester.onSetup();

        String nifDelete = "11111111A";

        // Introduzco los datos
        assertDoesNotThrow( () -> alumnoDAO.delAlumno(nifDelete));


        // Obtengo la base de datos actual
        IDataSet databaseDataSet = connection.createDataSet();
        ITable actualTable = databaseDataSet.getTable("alumnos");

        // Obtengo la base de datos del fichero
        IDataSet expectedDataSet = new FlatXmlDataFileLoader().load("/tabla4.xml");
        ITable expectedTable = expectedDataSet.getTable("alumnos");


        // Comprobaciones
        Assertion.assertEquals(expectedTable, actualTable);
    }

    @Test
    @Tag("Integracion-fase1")
    public void testA6() throws Exception{
        // Inicializamos la tabla2xml como base de datos
        IDataSet ds = new FlatXmlDataFileLoader().load("/tabla2.xml");
        databaseTester.setDataSet(ds);
        databaseTester.onSetup();


        String nifDelete = "33333333C";

        // Introduzco los datos
        DAOException e = assertThrows(DAOException.class,  () -> alumnoDAO.delAlumno(nifDelete));


        assertEquals("No se ha borrado ningun alumno", e.getMessage());

    }
}
