package Ejercicio2.TestLogin2;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestLogin2 {
    WebDriver driver;
    HomePage homePage;

    @BeforeEach
    public void setUp(){
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        homePage = new HomePage(driver);
    }

    @AfterEach
    public void close(){
        driver.close();
    }

    @Test
    public void test_Login_Correct(){
        assertEquals("Madison Island", homePage.getTitulo());
        CustomerLoginPage customerLogin = homePage.goLogin();
        assertEquals("Customer Login", customerLogin.getTitulo());
        MyAccountPage myAccountPage = customerLogin.login("sergio@barcelona.com", "123456");
        assertEquals("My Account", myAccountPage.getTitle());
    }

    @Test
    public void test_Login_Incorrect(){
        assertEquals("Madison Island", homePage.getTitulo());
        CustomerLoginPage customerLogin = homePage.goLogin();
        assertEquals("Customer Login", customerLogin.getTitulo());
        String error = customerLogin.loginErrorMsg("sergio2sdaads@barcelona.com", "123456");
        assertEquals("Invalid login or password.", error);
    }
}
