package Ejercicio1.sinPageObject;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestLogin {
    WebDriver driver;

    @BeforeEach
    public void setup(){
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("http://demo-store.seleniumacademy.com/");
    }

    @AfterEach
    public void close(){
        driver.close();
    }

    @Test
    public void loginOK(){
        String titulo = driver.getTitle();
        assertEquals("Madison Island", titulo);

        driver.findElement(By.cssSelector("#header > div > div.skip-links > div > a")).click();
        driver.findElement(By.cssSelector("#header-account > div > ul > li.last > a")).click();

        titulo = driver.getTitle();
        assertEquals("Customer Login", titulo);

        driver.findElement(By.cssSelector("#email")).sendKeys("sergio@barcelona.com");
        driver.findElement(By.cssSelector("#send2")).click();

        String requerido = driver.findElement(By.cssSelector("#advice-required-entry-pass")).getText();
        assertEquals("This is a required field.", requerido);



        driver.findElement(By.cssSelector("#pass")).sendKeys("123456");

        driver.findElement(By.cssSelector("#send2")).click();

        titulo = driver.getTitle();
        assertEquals("My Account", titulo);




    }

    @Test
    public void loginFailed(){
        String titulo = driver.getTitle();
        assertEquals("Madison Island", titulo);

        driver.findElement(By.cssSelector("#header > div > div.skip-links > div > a")).click();
        driver.findElement(By.cssSelector("#header-account > div > ul > li.last > a")).click();

        titulo = driver.getTitle();
        assertEquals("Customer Login", titulo);

        driver.findElement(By.cssSelector("#email")).sendKeys("test@barcelona.com");
        driver.findElement(By.cssSelector("#pass")).sendKeys("121781872711");

        driver.findElement(By.cssSelector("#send2")).click();

        String error = driver.findElement(By.cssSelector("body > div > div.page > div.main-container.col1-layout > div > div > div.account-login > ul > li > ul > li > span")).getText();
        assertEquals("Invalid login or password.", error);

    }
}
