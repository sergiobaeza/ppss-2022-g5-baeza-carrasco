package Ejercicio1.sinPageObject;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCreateAccount {
    WebDriver driver;

    @BeforeEach
    public void setup(){
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("http://demo-store.seleniumacademy.com/");
    }

    @Test
    @Tag("OnlyOnce")
    public void createAccount(){
        String titulo = driver.getTitle();
        assertEquals("Madison Island", titulo);

        driver.findElement(By.cssSelector("#header > div > div.skip-links > div > a")).click();

        driver.findElement(By.cssSelector("#header-account > div > ul > li.last > a")).click();

        titulo = driver.getTitle();
        assertEquals("Customer Login", titulo);

        driver.findElement(By.cssSelector("#login-form > div > div.col-1.new-users > div.buttons-set > a")).click();
        titulo = driver.getTitle();
        assertEquals("Create New Customer Account", titulo);

        driver.findElement(By.cssSelector("#firstname")).sendKeys("Sergio");
        driver.findElement(By.cssSelector("#middlename")).sendKeys("Midle");
        driver.findElement(By.cssSelector("#lastname")).sendKeys("Last");
        driver.findElement(By.cssSelector("#email_address")).sendKeys("sergio@barcelona.com");
        driver.findElement(By.cssSelector("#password")).sendKeys("123456");

        driver.findElement(By.cssSelector("#form-validate > div.buttons-set > button")).click();

        String text = driver.findElement(By.cssSelector("#advice-required-entry-confirmation")).getText();
        assertEquals("This is a required field.", text);


        driver.findElement(By.cssSelector("#confirmation")).sendKeys("123456");

        driver.findElement(By.cssSelector("#form-validate > div.buttons-set > button")).click();

        titulo = driver.getTitle();
        assertEquals("My Account", titulo);



    }


    @AfterEach
    public void close(){
        driver.close();
    }
}
