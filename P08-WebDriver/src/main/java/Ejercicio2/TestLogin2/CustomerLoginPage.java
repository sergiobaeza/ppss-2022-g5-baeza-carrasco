package Ejercicio2.TestLogin2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CustomerLoginPage {
    private WebDriver driver;
    WebElement mailInput;
    WebElement passwordInput;
    WebElement button;
    WebElement error;
    String titulo;


    public CustomerLoginPage(WebDriver driver){
        this.driver = driver;
        titulo = driver.getTitle();
        mailInput = driver.findElement(By.cssSelector("#email"));
        passwordInput = driver.findElement(By.cssSelector("#pass"));
        button = driver.findElement(By.cssSelector("#send2"));

    }

    public MyAccountPage login(String email, String password){
        mailInput.sendKeys(email);
        passwordInput.sendKeys(password);
        button.click();

        return new MyAccountPage(driver);
    }

    public String loginErrorMsg(String email, String password){
        mailInput.sendKeys(email);
        passwordInput.sendKeys(password);
        button.click();

        error = driver.findElement(By.cssSelector("body > div > div.page > div.main-container.col1-layout > div > div > div.account-login > ul > li > ul > li > span"));
        return error.getText();
    }

    public String getTitulo(){
        return titulo;
    }
}
