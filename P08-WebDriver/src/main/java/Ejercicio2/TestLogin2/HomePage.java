package Ejercicio2.TestLogin2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {
    private WebDriver driver;
    private String titulo;
    private WebElement account;
    private WebElement login;


    public HomePage(WebDriver driver){
        this.driver = driver;
        driver.get("http://demo-store.seleniumacademy.com/");
        titulo = driver.getTitle();
        account = driver.findElement(By.cssSelector("#header > div > div.skip-links > div > a"));
    }

    public CustomerLoginPage goLogin(){
        account.click();
        login = driver.findElement(By.cssSelector("#header-account > div > ul > li.last > a"));
        login.click();
        return new CustomerLoginPage(driver);
    }

    public String getTitulo(){
        return titulo;
    }
}
