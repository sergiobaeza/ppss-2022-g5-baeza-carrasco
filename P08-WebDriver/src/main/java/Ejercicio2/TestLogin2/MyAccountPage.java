package Ejercicio2.TestLogin2;

import org.openqa.selenium.WebDriver;

public class MyAccountPage {
    WebDriver driver;
    private String titulo;

    public MyAccountPage(WebDriver driver){
        this.driver = driver;
        titulo = driver.getTitle();
    }

    public String getTitle(){
        return titulo;
    }

}
