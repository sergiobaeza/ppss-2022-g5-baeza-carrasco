import java.time.LocalDate;

public class CalendarioStub2 extends Calendario{
    @Override
    public boolean es_festivo(LocalDate f){
        if(f.getDayOfMonth() == 20 || f.getDayOfMonth() == 24)
            return true;
        else
            return false;
    }
}
