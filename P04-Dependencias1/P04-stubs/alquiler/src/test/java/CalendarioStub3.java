import java.time.LocalDate;

public class CalendarioStub3 extends Calendario{
    @Override
    public boolean es_festivo(LocalDate f) throws CalendarioException{
        if(f.getDayOfMonth() == 18 || f.getDayOfMonth() == 21 || f.getDayOfMonth() == 22){
            throw new CalendarioException();
        }
        return false;
    }
}
