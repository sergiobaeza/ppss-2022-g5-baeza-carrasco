
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import java.time.LocalDate;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.*;
class AlquilaCochesTest {
    private TipoCoche tipo;
    private LocalDate fechaInicio;
    private int nDias;
    private AlquilaCoches alquilaCoches;
    private Calendario c;
    private Ticket t;
    private float res;
    private IService servicio;

    @BeforeEach
    private void inicializarServicio(){
        servicio = new ServicioStub(10);
    }

    @Test
    public void calculaPrecioC1(){
        tipo = TipoCoche.TURISMO;
        fechaInicio = LocalDate.of(2022, Month.MAY, 18);
        nDias = 10;
        c = new CalendarioStub1();
        res = 75;

        alquilaCoches = new AlquilaCochesTestable(c);


        assertDoesNotThrow(() -> t = alquilaCoches.calculaPrecio(tipo, fechaInicio, nDias, servicio), "Se ha producido una excepcion");
        assertEquals(75, t.getPrecio_final());

    }

    @Test
    public void calculaPrecioC2(){
        tipo = TipoCoche.CARAVANA;
        fechaInicio = LocalDate.of(2022, Month.JANUARY, 19);
        nDias = 7;
        c = new CalendarioStub2();

        res = 75;

        alquilaCoches = new AlquilaCochesTestable(c);

        assertDoesNotThrow(() -> t = alquilaCoches.calculaPrecio(tipo, fechaInicio, nDias, servicio), "Se ha producido una excepcion");
        assertEquals(62.5, t.getPrecio_final());

    }

    @Test
    public void calculaPrecioC3(){
        tipo = TipoCoche.TURISMO;
        fechaInicio = LocalDate.of(2022, Month.APRIL, 17);
        nDias = 8;
        c = new CalendarioStub3();


        alquilaCoches = new AlquilaCochesTestable(c);

        MensajeException e = assertThrows(MensajeException.class, () -> alquilaCoches.calculaPrecio(tipo, fechaInicio, nDias, servicio));
        assertEquals("Error en dia: 2022-04-18; Error en dia: 2022-04-21; Error en dia: 2022-04-22; ", e.getMessage());

    }

}