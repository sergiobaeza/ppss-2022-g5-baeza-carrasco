public class MensajeException extends Exception{
    private String msg;
    public MensajeException(String msg){
        this.msg = msg;
    }

    @Override
    public String getMessage(){
        return msg;
    }

}
