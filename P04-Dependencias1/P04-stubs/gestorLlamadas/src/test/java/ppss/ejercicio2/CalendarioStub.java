package ppss.ejercicio2;

public class CalendarioStub extends Calendario {
    int res;

    public CalendarioStub(int res){
        this.res = res;
    }

    @Override
    public int getHoraActual() {
        return res;
    }
}
