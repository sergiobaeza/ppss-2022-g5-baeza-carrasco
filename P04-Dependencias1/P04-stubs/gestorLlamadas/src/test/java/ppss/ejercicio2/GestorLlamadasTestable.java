package ppss.ejercicio2;

public class GestorLlamadasTestable extends GestorLlamadas{
    private Calendario c;

    public GestorLlamadasTestable(Calendario c){
        this.c = c;
    }

    @Override
    public Calendario getCalendario(){
        return c;
    }

}
