package ppss.ejercicio1;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GestorLlamadasTest {
    int min;
    int hora;
    double resEsperado;
    GestorLlamadas g;

    @Test
    public void calculaConsumoC1(){
        min = 10;
        hora = 15;
        resEsperado = 208;

        g = new GestorLlamadasTestable(hora);
        double res = g.calculaConsumo(min);

        assertEquals(resEsperado, res);
    }

    @Test
    public void calculaConsumoC2(){
        min = 10;
        hora = 22;
        resEsperado = 105;

        g = new GestorLlamadasTestable(hora);
        double res = g.calculaConsumo(min);

        assertEquals(resEsperado, res);
    }

}