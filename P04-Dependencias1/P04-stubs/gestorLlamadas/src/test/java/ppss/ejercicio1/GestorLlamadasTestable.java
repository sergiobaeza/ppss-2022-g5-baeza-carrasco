package ppss.ejercicio1;

public class GestorLlamadasTestable extends GestorLlamadas{
    private int resultado;

    public GestorLlamadasTestable(int res){
        this.resultado = res;
    }

    @Override
    public int getHoraActual(){
        return resultado;
    }
}
