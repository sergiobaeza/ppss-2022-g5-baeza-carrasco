package ppss.P04;

public class OperacionFactoria {
    private static Operacion operacion = null;

    public static Operacion create(){
        if(operacion != null){
            return operacion;
        }
        else{
            return new Operacion();
        }
    }

    public static void setOperacion(Operacion o){
        operacion = o;
    }

    public static void resetOperacion(){
        operacion = null;
    }

}
