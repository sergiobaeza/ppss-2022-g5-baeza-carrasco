package ppss.P04;
import ppss.P04.excepciones.*;

//paquete ppss.P04
public interface IOperacionBO {
    public void operacionReserva(String socio, String isbn) throws IsbnInvalidoException, JDBCException, SocioInvalidoException;
}