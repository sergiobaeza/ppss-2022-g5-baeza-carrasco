package ppss.P04;
import ppss.P04.excepciones.IsbnInvalidoException;
import ppss.P04.excepciones.JDBCException;
import ppss.P04.excepciones.SocioInvalidoException;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;

public class OperacionStub extends Operacion{
    private int state;

    public OperacionStub(int s){
        state = s;
    }

    public void operacionReserva(String socio, String isbn) throws IsbnInvalidoException, JDBCException, SocioInvalidoException {
        if (state == 3){
            throw new IsbnInvalidoException();
        }
        else if (state == 4){
            throw new SocioInvalidoException();
        }
        else if(state == 5){
            throw new JDBCException();
        }
    }


}
