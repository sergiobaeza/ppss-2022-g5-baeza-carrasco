package ppss.P04;

import org.junit.jupiter.api.Test;
import ppss.P04.excepciones.ReservaException;

import static org.junit.jupiter.api.Assertions.*;

class ReservaTest {
    private String login, password, socio;
    private String[] isbns;
    private int statusDb;
    private Reserva r;

    @Test
    public void realizaReservaC1(){
        login = "xxxx";
        password = "xxxx";
        socio = "Luis";
        statusDb = 1;
        isbns = new String[]{"11111"};

        r = new ReservaTestable();

        ReservaException e = assertThrows(ReservaException.class, () -> r.realizaReserva(login, password, socio, isbns));
        assertEquals("ERROR de permisos; ", e.getMessage());
    }

    @Test
    public void realizaReservaC2(){
        login = "ppss";
        password = "ppss";
        socio = "Luis";
        statusDb = 2;
        isbns = new String[]{"11111", "22222"};
        OperacionFactoria.setOperacion(new OperacionStub(statusDb));
        r = new ReservaTestable();
        assertDoesNotThrow( () -> r.realizaReserva(login, password, socio, isbns), "Lanza excepcion");
    }

    @Test
    public void realizaReservaC3(){
        login = "ppss";
        password = "ppss";
        socio = "Luis";
        statusDb = 3;
        isbns = new String[]{"33333"};
        OperacionFactoria.setOperacion(new OperacionStub(statusDb));
        r = new ReservaTestable();

        ReservaException e = assertThrows(ReservaException.class, () -> r.realizaReserva(login, password, socio, isbns));
        assertEquals("ISBN invalido:33333; ", e.getMessage());
    }


    @Test
    public void realizaReservaC4(){
        login = "ppss";
        password = "ppss";
        socio = "Pepe";
        statusDb = 4;
        isbns = new String[]{"33333"};
        OperacionFactoria.setOperacion(new OperacionStub(statusDb));
        r = new ReservaTestable();

        ReservaException e = assertThrows(ReservaException.class, () -> r.realizaReserva(login, password, socio, isbns));
        assertEquals("SOCIO invalido; ", e.getMessage());
    }

    @Test
    public void realizaReservaC5(){
        login = "ppss";
        password = "ppss";
        socio = "Luis";
        statusDb = 5;
        isbns = new String[]{"33333"};
        OperacionFactoria.setOperacion(new OperacionStub(statusDb));
        r = new ReservaTestable();

        ReservaException e = assertThrows(ReservaException.class, () -> r.realizaReserva(login, password, socio, isbns));
        assertEquals("CONEXION invalida; ", e.getMessage());
    }
}