package ppss;

import org.easymock.EasyMock;
import static org.easymock.EasyMock.*;
import org.easymock.Mock;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GestorLlamadasTest {
    private Calendario dep;
    private GestorLlamadas dep2;
    double res, resEsperado;

    @Test
    public void calculaConsumoC1(){
        dep = EasyMock.mock(Calendario.class);
        EasyMock.expect(dep.getHoraActual()).andReturn(10);
        EasyMock.replay(dep);

        dep2 = partialMockBuilder(GestorLlamadas.class).addMockedMethod("getCalendario").mock();
        EasyMock.expect(dep2.getCalendario()).andReturn(dep);
        EasyMock.replay(dep2);

        resEsperado = 457.6;
        res = dep2.calculaConsumo(22);

        assertEquals(resEsperado, res);
        EasyMock.verify(dep, dep2);
    }

    @Test
    public void calculaConsumoC2(){
        dep = EasyMock.mock(Calendario.class);
        EasyMock.expect(dep.getHoraActual()).andReturn(21);
        EasyMock.replay(dep);

        dep2 = partialMockBuilder(GestorLlamadas.class).addMockedMethod("getCalendario").mock();
        EasyMock.expect(dep2.getCalendario()).andReturn(dep);
        EasyMock.replay(dep2);

        resEsperado = 136.5;
        res = dep2.calculaConsumo(13);

        assertEquals(resEsperado, res);
        EasyMock.verify(dep, dep2);
    }

}