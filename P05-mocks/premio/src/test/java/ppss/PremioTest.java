package ppss;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.easymock.EasyMock;
import static org.easymock.EasyMock.*;

import static org.junit.jupiter.api.Assertions.*;

class PremioTest {
    private ClienteWebService dep1;
    private Premio dep2;
    private String res, resEsperado;

    @Test
    public void compruebaPremioC1() {
        dep1 = EasyMock.strictMock(ClienteWebService.class);
        Assertions.assertDoesNotThrow( () -> EasyMock.expect(dep1.obtenerPremio(2)).andReturn("entrada final Champions"));

        EasyMock.expect(dep1.obtenerPremio((2))).andThrow(new RuntimeException());
        Assertions.assertDoesNotThrow( () -> EasyMock.expect(dep1.obtenerPremio(1)).andReturn("entrada final Champions"));
        EasyMock.replay(dep1);

        dep2 = partialMockBuilder(Premio.class).addMockedMethod("generaNumero").strictMock();


        dep2.cliente = dep1;
        EasyMock.replay(dep2);

        res = dep2.compruebaPremio();
        resEsperado = "Premiado con entrada final Champions";
        assertEquals(resEsperado, res);

        EasyMock.verify(dep1, dep2);

    }

    /*
    public void compruebaPremioC2() {
        dep1 = EasyMock.mock(ClienteWebService.class);
        assertDoesNotThrow(() -> EasyMock.expect(dep1.obtenerPremio()).andThrow(new ClienteWebServiceException()));
        EasyMock.replay(dep1);

        dep2 = partialMockBuilder(Premio.class).addMockedMethod("generaNumero").mock();
        EasyMock.expect(dep2.generaNumero()).andReturn((float) 0.03);
        dep2.cliente = dep1;
        EasyMock.replay(dep2);

        res = dep2.compruebaPremio();
        resEsperado = "No se ha podido obtener el premio";
        assertEquals(resEsperado, res);

        EasyMock.verify(dep1, dep2);

    }

    @Test
    public void compruebaPremioC3() {
        dep2 = partialMockBuilder(Premio.class).addMockedMethod("generaNumero").mock();
        EasyMock.expect(dep2.generaNumero()).andReturn((float) 0.3);
        EasyMock.replay(dep2);

        res = dep2.compruebaPremio();
        resEsperado = "Sin premio";
        assertEquals(resEsperado, res);

        EasyMock.verify(dep2);

    }*/
}