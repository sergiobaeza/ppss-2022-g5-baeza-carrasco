package ppss;

public class FicheroException extends Exception{
    String message;
    public FicheroException(String string){
        this.message = string;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
