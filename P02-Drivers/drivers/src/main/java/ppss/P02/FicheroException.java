package ppss.P02;

public class FicheroException extends Exception {
    public FicheroException() {
    }

    public FicheroException(String message) {
        super(message);
    }
}
