package ppss.P02;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Tag;


import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import ppss.P02.Cine;

import static org.junit.jupiter.api.Assertions.*;

class CineTest {
    boolean[] asientos;
    boolean[] asientosEsperados;
    Cine c;
    boolean res;
    boolean resEsperado;
    int solicitados;

    @BeforeEach
    private void inicializarCine(){
        c = new Cine();
    }

    @Test
    public void reservaButacasC1(){
        // Datos de inicializacion
        asientos = new boolean[0];
        solicitados = 3;
        res = c.reservaButacasV1(asientos, solicitados);

        // Esperado
        resEsperado = false;
        asientosEsperados = new boolean[0];

        // Comprobacion
        assertAll("GrupoTestC1",
                () -> assertEquals(res, resEsperado),
                () -> assertArrayEquals(asientos, asientosEsperados));

    }

    @Test
    public void reservaButacasC2(){
        // Datos de inicializacion
        asientos = new boolean[0];
        solicitados = 0;
        res = c.reservaButacasV1(asientos, solicitados);

        // Esperado
        resEsperado = false;
        asientosEsperados = new boolean[0];

        // Comprobacion
        assertAll("GrupoTestC2",
                () -> assertEquals(res, resEsperado),
                () -> assertArrayEquals(asientos, asientosEsperados));

    }


    @Test
    public void reservaButacasC3(){
        // Datos de inicializacion
        asientos = new boolean[]{false, false, false, true, true};
        solicitados = 2;
        res = c.reservaButacasV1(asientos, solicitados);

        // Esperado
        resEsperado = true;
        asientosEsperados = new boolean[]{true, true, false, true, true};

        // Comprobacion
        assertAll("GrupoTestC3",
                () -> assertEquals(res, resEsperado),
                () -> assertArrayEquals(asientos, asientosEsperados));

    }


    @Test
    public void reservaButacasC4(){
        // Datos de inicializacion
        asientos = new boolean[]{true, true, true};
        solicitados = 1;
        res = c.reservaButacasV1(asientos, solicitados);

        // Esperado
        resEsperado = false;
        asientosEsperados = new boolean[]{true, true, true};

        // Comprobacion
        assertAll("GrupoTestC4",
                () -> assertEquals(res, resEsperado),
                () -> assertArrayEquals(asientos, asientosEsperados));

    }


    @ParameterizedTest(name = "Probando a reservar {1} usuarios con el array vacio")
    @Tag("parametrizado")
    @ValueSource(ints = {0, 1, 2, 3, 4, 5})
    public void reservaButacasC5(int solicitados){
        // Datos de inicializacion
        asientos = new boolean[0];
        res = c.reservaButacasV1(asientos, solicitados);

        // Esperado
        resEsperado = false;
        asientosEsperados = new boolean[0];

        // Comprobacion
        assertAll("GrupoTestC1",
                () -> assertEquals(res, resEsperado),
                () -> assertArrayEquals(asientos, asientosEsperados));
    }


}