package ppss.P02;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.xml.crypto.Data;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class DataArrayTest {
    DataArray dt;
    int[] res;
    int[] resExcepted;

    @Test
    public void deleteC1(){
        dt = new DataArray(new int[]{1,3,5,7});
        assertDoesNotThrow(() -> dt.delete(5), "Excepcion lanzada");
        res = dt.getColeccion();
        resExcepted = new int[]{1,3,7};
        assertAll("GrupoTestC1",
                () -> assertArrayEquals(resExcepted, res),
                () -> assertEquals(resExcepted.length, res.length));
    }

    @Test
    public void deleteC2(){
        dt = new DataArray(new int[]{1,3,3,5,7});
        assertDoesNotThrow(() -> dt.delete(3), "Excepcion lanzada");
        res = dt.getColeccion();
        resExcepted = new int[]{1,3,5,7};
        assertAll("GrupoTestC2",
                () -> assertArrayEquals(resExcepted, res),
                () -> assertEquals(resExcepted.length, res.length));
    }

    @Test
    public void deleteC3(){
        dt = new DataArray(new int[]{1,2,3,4,5,6,7,8,9,10});
        assertDoesNotThrow(() -> dt.delete(4), "Excepcion lanzada");
        res = dt.getColeccion();
        resExcepted = new int[]{1,2,3,5,6,7,8,9,10};
        assertAll("GrupoTestC3",
                () -> assertArrayEquals(resExcepted, res),
                () -> assertEquals(resExcepted.length, res.length));
    }

    @Test
    public void deleteC4(){
        dt = new DataArray();

        DataException e = assertThrows(DataException.class, () -> dt.delete(8));
        assertEquals("No hay elementos en la colección", e.getMessage());
    }

    @Test
    public void deleteC5(){
        dt = new DataArray(new int[]{1,3,5,7});
        DataException e = assertThrows(DataException.class, () -> dt.delete(-5));
        assertEquals("El valor a borrar debe ser > 0", e.getMessage());
    }

    @Test
    public void deleteC6(){
        dt = new DataArray();
        DataException e = assertThrows(DataException.class, () -> dt.delete(0));
        assertEquals("Colección vacía. Y el valor a borrar debe ser > 0", e.getMessage());
    }

    @Test
    public void deleteC7(){
        dt = new DataArray(new int[]{1,3,5,7});
        DataException e = assertThrows(DataException.class, () -> dt.delete(8));
        assertEquals("Elemento no encontrado", e.getMessage());
    }

    @ParameterizedTest
    @MethodSource("casosDePruebaC8")
    @Tag("parametrizado")
    @Tag("conExcepciones")
    public void testParametrizadoC8(int[] inside, int delete, String salida){
        dt = new DataArray(inside);
        DataException e =assertThrows(DataException.class, () -> dt.delete(delete));
        assertEquals(salida, e.getMessage());
    }

    // Argumentos para el test de arriba ^^^^
    private static Stream<Arguments> casosDePruebaC8() {
        return Stream.of(
                Arguments.of(new int[]{}, 8, "No hay elementos en la colección"),
                Arguments.of(new int[]{1, 3, 5, 7}, -5, "El valor a borrar debe ser > 0"),
                Arguments.of(new int[]{}, 0, "Colección vacía. Y el valor a borrar debe ser > 0"),
                Arguments.of(new int[]{1,3,5,7}, 8, "Elemento no encontrado"));

    }

    @ParameterizedTest
    @MethodSource("casosDePruebaC9")
    @Tag("parametrizado")
    public void testParametrizadoC9(int[] inside, int[] resEsp, int delete){
        dt = new DataArray(inside);
        assertDoesNotThrow(() -> dt.delete(delete), "Excepcion lanzada");
        res = dt.getColeccion();
        assertAll("GrupoTestC9",
                () -> assertArrayEquals(resEsp, res),
                () -> assertEquals(resEsp.length, res.length));
    }

    // Argumentos para el test de arriba ^^^^
    private static Stream<Arguments> casosDePruebaC9(){
        return Stream.of(
                        Arguments.of(new int[]{1,3,5,7}, new int[]{1,3,7}, 5),
                        Arguments.of(new int[]{1,3,3,5,7}, new int[]{1,3,5,7}, 3),
                        Arguments.of(new int[]{1,2,3,4,5,6,7,8,9,10}, new int[]{1,2,3,5,6,7,8,9,10}, 4));
    }





}