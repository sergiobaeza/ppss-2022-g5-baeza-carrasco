package ppss.P02;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Tag;

import static org.junit.jupiter.api.Assertions.*;

class FicheroTextoTest {
    String nombreFichero;
    FicheroTexto ft;
    int res;
    int resEsperado;

    @BeforeEach
    private void inicializarFichero(){
        ft = new FicheroTexto();
    }

    @Test
    public void contarCaracteresC1(){
        nombreFichero = "ficheroC1.txt";

        FicheroException e = assertThrows(FicheroException.class, () -> ft.contarCaracteres(nombreFichero));
        assertEquals("ficheroC1.txt (No existe el archivo o el directorio)", e.getMessage());
    }

    @Test
    public void contarCaracteresC2(){
        nombreFichero = "src/test/resources/ficheroCorrecto.txt";
        resEsperado = 3;

        assertDoesNotThrow(() -> res = ft.contarCaracteres(nombreFichero), "Se ha producido una excepcion");
        assertEquals(resEsperado, res);
    }

    @Test
    @Tag("excluido")
    public void contarCaracteresC3(){
        fail("Completa el test");
    }

    @Test
    @Tag("excluido")
    public void contarCaracteresC4(){
        fail("Completa el test");
    }

}